package com.example.asus.model;

import java.io.Serializable;

/**
 * Created by ASUS on 7/27/2017.
 */

public class Lop implements Serializable {
    private int Stt;
    private  String MaLop;
    private  String TenLop;

    public Lop() {
    }

    public Lop(int stt, String maLop, String tenLop) {
        Stt = stt;
        MaLop = maLop;
        TenLop = tenLop;
    }

    public int getStt() {
        return Stt;
    }

    public void setStt(int stt) {
        Stt = stt;
    }

    public String getMaLop() {
        return MaLop;
    }

    public void setMaLop(String maLop) {
        MaLop = maLop;
    }

    public String getTenLop() {
        return TenLop;
    }

    public void setTenLop(String tenLop) {
        TenLop = tenLop;
    }

    @Override
    public String toString() {
        return "\t"+"\t"+"\t"+"\t"+this.Stt+"\t"+"\t"+"\t"+"\t"+"\t"+"\t"+"\t"+"\t"+this.MaLop+"\t"+"\t"+"\t"+this.TenLop;
    }
}
