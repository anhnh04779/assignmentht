package com.example.asus.model;

import java.io.Serializable;

/**
 * Created by ASUS on 7/29/2017.
 */

public class LichHoc implements Serializable {
    private String lopHoc,monHoc,ngay,thoiGianBatDau,thoiGianKetThuc;

    public LichHoc() {
    }

    public LichHoc(String lopHoc, String monHoc, String ngay, String thoiGianBatDau, String thoiGianKetThuc) {
        this.lopHoc = lopHoc;
        this.monHoc = monHoc;
        this.ngay = ngay;
        this.thoiGianBatDau = thoiGianBatDau;
        this.thoiGianKetThuc = thoiGianKetThuc;
    }

    public String getLopHoc() {
        return lopHoc;
    }

    public void setLopHoc(String lopHoc) {
        this.lopHoc = lopHoc;
    }

    public String getMonHoc() {
        return monHoc;
    }

    public void setMonHoc(String monHoc) {
        this.monHoc = monHoc;
    }

    public String getNgay() {
        return ngay;
    }

    public void setNgay(String ngay) {
        this.ngay = ngay;
    }

    public String getThoiGianBatDau() {
        return thoiGianBatDau;
    }

    public void setThoiGianBatDau(String thoiGianBatDau) {
        this.thoiGianBatDau = thoiGianBatDau;
    }

    public String getThoiGianKetThuc() {
        return thoiGianKetThuc;
    }

    public void setThoiGianKetThuc(String thoiGianKetThuc) {
        this.thoiGianKetThuc = thoiGianKetThuc;
    }

    @Override
    public String toString() {
        return this.lopHoc+"\t"+this.monHoc+"\t"+"\t"+"\t"+"\t"+this.ngay+"\t"+"\t"+this.thoiGianBatDau+"-"+this.thoiGianKetThuc;
    }
}
