package com.example.asus.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import com.example.asus.asignment.R;
import com.example.asus.model.DoiTuongKhenThuong;

import java.util.List;

/**
 * Created by ASUS on 7/30/2017.
 */

public class KhenThuongAdapter extends ArrayAdapter<DoiTuongKhenThuong> {
    Activity context; @LayoutRes int resource; @NonNull List<DoiTuongKhenThuong> objects;
    public KhenThuongAdapter(@NonNull Activity context, @LayoutRes int resource, @NonNull List<DoiTuongKhenThuong> objects) {
        super(context, resource, objects);
        this.context=context;
        this.resource=resource;
        this.objects=objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater=this.context.getLayoutInflater();
        View row=inflater.inflate(this.resource,null);
        EditText txtMaKT= (EditText) row.findViewById(R.id.txtMaKT);
        EditText txtTenKT= (EditText) row.findViewById(R.id.txtTenKT);
        EditText txtNoiDungKT= (EditText) row.findViewById(R.id.txtNoiDungKT);
        EditText txtPhanThuong= (EditText) row.findViewById(R.id.txtPhanThuong);
        DoiTuongKhenThuong doiTuongKhenThuong= this.objects.get(position);
        txtMaKT.setText(doiTuongKhenThuong.getMa());
        txtTenKT.setText(doiTuongKhenThuong.getTen());
        txtNoiDungKT.setText(doiTuongKhenThuong.getNoiDung());
        txtPhanThuong.setText(doiTuongKhenThuong.getPhanThuong());
        return row;
    }
}
