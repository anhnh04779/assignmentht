package com.example.asus.model;

import java.io.Serializable;

/**
 * Created by ASUS on 7/30/2017.
 */

public class DoiTuongKiLuat implements Serializable {
    private String ma;
    private String ten;
    private String noiDung;
    private String hinhthuckl;
    public DoiTuongKiLuat() {
    }

    public DoiTuongKiLuat(String ma, String ten, String noiDung,String hinhthuckl) {
        this.ma = ma;
        this.ten = ten;
        this.noiDung = noiDung;
        this.hinhthuckl=hinhthuckl;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getNoiDung() {
        return noiDung;
    }

    public void setNoiDung(String noiDung) {
        this.noiDung = noiDung;
    }

    public String getHinhthuckl() {
        return hinhthuckl;
    }

    public void setHinhthuckl(String hinhthuckl) {
        this.hinhthuckl = hinhthuckl;
    }
}
