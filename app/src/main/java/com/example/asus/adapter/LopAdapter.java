package com.example.asus.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.asus.asignment.R;
import com.example.asus.model.Lop;

import java.util.List;

/**
 * Created by ASUS on 8/10/2017.
 */

public class LopAdapter extends ArrayAdapter<Lop> {
    Activity context; @LayoutRes int resource; @NonNull List<Lop> objects;
    public LopAdapter(@NonNull Activity context, @LayoutRes int resource, @NonNull List<Lop> objects) {
        super(context, resource, objects);
        this.context=context;
        this.resource=resource;
        this.objects=objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater=this.context.getLayoutInflater();
        View row=inflater.inflate(this.resource,null);
        TextView tvStt= (TextView) row.findViewById(R.id.tvstt);
        TextView tvMaLop= (TextView) row.findViewById(R.id.tvMaLop);
        TextView tvTenLop= (TextView) row.findViewById(R.id.tvTenLop);
        Lop lop= this.objects.get(position);
        tvStt.setText(String.valueOf(position+1));
        tvMaLop.setText(lop.getMaLop());
        tvTenLop.setText(lop.getTenLop());
        return row;
    }
}
